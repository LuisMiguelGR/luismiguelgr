import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {

// tslint:disable-next-line: max-line-length
public url = 'https://firebasestorage.googleapis.com/v0/b/luismiguelgr-3f9f3.appspot.com/o/CVLuisMiguelGR.pdf?alt=media&token=4c957ff8-db75-47d4-a499-423f976bb1e6';

  constructor(
  ) { }

  ngOnInit() {
  }

  abrirCv(){
    window.open(this.url);
  }

  abrirUrl(pagina: string){
    window.open(pagina);
  }
}
