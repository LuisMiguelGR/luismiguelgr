// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyB_KpUH4iWREfOv0xgj60ThUtE2vJDXwYQ',
    authDomain: 'luismiguelgr-3f9f3.firebaseapp.com',
    databaseURL: 'https://luismiguelgr-3f9f3.firebaseio.com',
    projectId: 'luismiguelgr-3f9f3',
    storageBucket: 'luismiguelgr-3f9f3.appspot.com',
    messagingSenderId: '101695441400',
    appId: '1:101695441400:web:e714554464a0d3a2'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
